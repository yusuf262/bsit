package com.yusuf.bsit.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.yusuf.bsit.databinding.FragmentTransactionBinding
import com.yusuf.bsit.model.ContactResponse
import com.yusuf.bsit.model.TransactionResponse
import com.yusuf.bsit.presentation.adapter.TransactionAdapter
import com.yusuf.bsit.presentation.fragment.viewmodel.ProfileViewModel
import com.yusuf.bsit.presentation.fragment.viewmodel.TransactionViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TransactionFragment : Fragment(){
    private var _binding: FragmentTransactionBinding? = null
    private val binding get() = _binding
    private var transactionAdapter = TransactionAdapter()
    private val viewModel: TransactionViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTransactionBinding.inflate(
            inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewModel.getTransaction()
    }

    private fun observeViewModel() {
        viewModel.transaction.observe(viewLifecycleOwner) { contactData ->
            if (contactData != null) {
                setData(contactData)
            }
        }
    }

    private fun setData(data: List<TransactionResponse>) {
        transactionAdapter.setData(data.toMutableList())
        binding?.rvListTransaction?.adapter = transactionAdapter
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}