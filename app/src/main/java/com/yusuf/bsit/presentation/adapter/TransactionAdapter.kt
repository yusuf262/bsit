package com.yusuf.bsit.presentation.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.yusuf.bsit.databinding.ItemTransactionBinding
import com.yusuf.bsit.model.ContactResponse
import com.yusuf.bsit.model.TransactionResponse

class TransactionAdapter(
): RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {


    private var data: MutableList<TransactionResponse> = mutableListOf()
    inner class TransactionViewHolder(private val binding: ItemTransactionBinding
    ): RecyclerView.ViewHolder(binding.root){
        fun bindingView(item: TransactionResponse){
            binding.ccTvTitle.text = item.name
            binding.ccTvSubtitle.text = item.metodeTrf
            Glide
                .with(binding.root.context)
                .load(item.imageUrl)
                .centerCrop()
                .into(binding.ccImage)
            binding.ccPrice.text = "Rp${item.nominalSaldo}"
            if (item.flagDebitCredit == 1){
                binding.ccPrice.setTextColor(Color.RED)
            } else {
                binding.ccPrice.setTextColor(Color.GREEN)
            }
        }
    }
    fun  setData(newData: MutableList<TransactionResponse>){
        data =newData
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= TransactionViewHolder(
        ItemTransactionBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun getItemCount() = data.size


    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        holder.bindingView(data[position])
    }
}