package com.yusuf.bsit.presentation.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.bumptech.glide.Glide
import com.yusuf.bsit.databinding.FragmentProfileBinding
import com.yusuf.bsit.model.ProfileResponse
import com.yusuf.bsit.presentation.fragment.viewmodel.ProfileViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProfileFragment : Fragment(){
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding

    private val viewModel: ProfileViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProfileBinding.inflate(
            inflater, container, false)
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeViewModel()
        viewModel.getProfile()
    }

    private fun observeViewModel(){
        viewModel.profile.observe(viewLifecycleOwner){
           initViewProfile(it)
        }
    }

    private fun initViewProfile(data: ProfileResponse){
        binding?.ccTvName?.text = data.name
        binding?.ccTvJoinDate?.text = data.joinedDate
        binding?.ccImage?.let {
            Glide
                .with(context ?: return)
                .load(data.imageUrl)
                .circleCrop()
                .into(it)
        }

        binding?.ccTvStatus?.text =
            (if (data.status ==1) "Available" else "Not Available")

        binding?.ccTvNumber?.text = data.noTelp
        binding?.ccGoLocation?.setOnClickListener {
            seeLocation(data.lat.toString(), data.lng.toString())
        }

    }

    private fun seeLocation(lat: String, lng: String){
        //val uri = Uri.parse("https://www.google.com/maps/search/?api=1&query=$lat,%20$lng")
//        val uri = Uri.parse("google.navigation:q=$lat,$lng")
        val uri = Uri.parse("google.streetview:cbll=$lat, $lng")
        val intent = Intent(Intent.ACTION_VIEW, uri)
        context?.startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}