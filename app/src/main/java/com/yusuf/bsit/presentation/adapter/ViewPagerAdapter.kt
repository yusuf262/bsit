package com.yusuf.bsit.presentation.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

//class DemoCollectionPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
//
//    override fun getCount(): Int  = 100
//
//    override fun getItem(i: Int): Fragment {
//        val fragment = DemoObjectFragment()
//        fragment.arguments = Bundle().apply {
//            // Our object is just an integer 😛
//            putInt(ARG_OBJECT, i + 1)
//        }
//        return fragment
//    }
//
//    override fun getPageTitle(position: Int): CharSequence {
//        return "OBJECT ${(position + 1)}"
//    }
//}
class ViewPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
    private val lisFragment: MutableList<Fragment> = mutableListOf()
    private val listTitle: MutableList<String> = mutableListOf()
    fun addFragment(fragment: Fragment, title: String ){
        lisFragment.add(fragment)
        listTitle.add(title)
        notifyDataSetChanged()
    }
    override fun getCount(): Int {
        return lisFragment.size
    }

    override fun getItem(position: Int): Fragment {
        return lisFragment[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return listTitle[position]
    }
}