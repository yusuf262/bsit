package com.yusuf.bsit.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import com.google.android.material.tabs.TabLayoutMediator
import com.yusuf.bsit.R
import com.yusuf.bsit.databinding.ActivityMainBinding
import com.yusuf.bsit.model.ContactResponse
import com.yusuf.bsit.model.TransactionResponse
import com.yusuf.bsit.presentation.adapter.BsitFragmentPagerAdapter
import com.yusuf.bsit.presentation.adapter.TransactionAdapter
import com.yusuf.bsit.presentation.adapter.ViewPagerAdapter
import com.yusuf.bsit.presentation.fragment.ContactsFragment
import com.yusuf.bsit.presentation.fragment.ProfileFragment
import com.yusuf.bsit.presentation.fragment.TransactionFragment
import com.yusuf.bsit.presentation.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewPagerAdapter: ViewPagerAdapter
    private val mainViewModel by viewModels<MainViewModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setUpViewPager(listOf())
        setUpTabLayout()
        observeViewModel()
        mainViewModel.getTransaction()
        mainViewModel.getContact()
    }

//    private fun setData(dataTransaction: List<TransactionResponse>, dataContact: List<ContactResponse>){
//        val bundle = listOf(
//            dataContact.let { ContactsFragment.createInstance(it) }
//        )
//        setUpViewPager(bundle)
//    }
    private fun setUpViewPager(bundle: List<Bundle>){
        val viewPager = binding.vpContainer
        viewPager.adapter = BsitFragmentPagerAdapter(this,bundle)
    }

    private fun setUpTabLayout(){
        val tabLayout = binding.tabsFragment
        val viewPager = binding.vpContainer
        TabLayoutMediator(tabLayout, viewPager){
            tab, position ->
            when(position){
                0 -> tab.text = "Transaction"
                1 -> tab.text = "Contact"
                2 -> tab.text = "Profile"
            }
        }.attach()
    }

    private fun observeViewModel(){
        mainViewModel.transaction.observe(this){
//            setDataForTransaction(it)
            //setData(null, this)
        }
        mainViewModel.contact.observe(this){

        }
        mainViewModel.errorMessage.observe(this){
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        }
        mainViewModel.showLoading.observe(this){ isShow ->
            if (isShow) showLoading() else hideLoading()
        }
    }

//    private fun setDataForTransaction(data: List<TransactionResponse>){
//        val adapter = TransactionAdapter(data)
////        binding.rvListTransaction.adapter = adapter
//    }

    private fun showLoading(){
        binding.cmpLoading.root.visibility = View.VISIBLE
//        binding.rvListTransaction.visibility = View.GONE
    }

    private fun hideLoading(){
        binding.cmpLoading.root.visibility = View.GONE
//        binding.rvListTransaction.visibility = View.VISIBLE
    }

    private fun loadFragment(){
        val transactionFragment = TransactionFragment()
        val contactsFragment = ContactsFragment()
        val profileFragment = ProfileFragment()

        viewPagerAdapter.addFragment(transactionFragment, "Transaction")
        viewPagerAdapter.addFragment(contactsFragment, "Contacts")
        viewPagerAdapter.addFragment(profileFragment, "Profile")


    }
}