package com.yusuf.bsit.presentation.fragment

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.yusuf.bsit.databinding.FragmentContactsBinding
import com.yusuf.bsit.model.ContactResponse
import com.yusuf.bsit.presentation.adapter.ContactsAdapter
import com.yusuf.bsit.presentation.fragment.viewmodel.ContactViewModel
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.lifecycle.HiltViewModel

@AndroidEntryPoint
class ContactsFragment : Fragment() {
    private var _binding: FragmentContactsBinding? = null
    private val binding get() = _binding
    private var contactsAdapter = ContactsAdapter()
    private val viewModel: ContactViewModel by viewModels(
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentContactsBinding.inflate(
            inflater,
            container,
            false
        )
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        viewModel.getContact()
        binding?.etSearch?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val editTextValue = binding?.etSearch?.text
                if (editTextValue?.isEmpty() == true){
                    binding?.ivClose?.visibility = View.INVISIBLE
                } else {
                    binding?.ivClose?.visibility = View.VISIBLE
                }
            }

            override fun afterTextChanged(p0: Editable?) {

                viewModel.searchContact(p0.toString())
            }

        })

        binding?.ivClose?.setOnClickListener {
            binding?.etSearch?.text?.clear()
        }
    }

    private fun observeViewModel() {
        viewModel.contact.observe(viewLifecycleOwner) { contactData ->
            if (contactData != null) {
                setData(contactData)
            }
        }
    }

    private fun setData(data: List<ContactResponse>) {
        contactsAdapter.setData(data.toMutableList())
        contactsAdapter.onClickIconCall {
            callAFriend(it.noTelp)
        }
        binding?.rvListContacts?.adapter = contactsAdapter
    }

    private fun callAFriend(phoneNumber: String?) {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phoneNumber")
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}