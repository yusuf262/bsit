package com.yusuf.bsit.presentation.adapter

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.yusuf.bsit.presentation.fragment.ContactsFragment
import com.yusuf.bsit.presentation.fragment.ProfileFragment
import com.yusuf.bsit.presentation.fragment.TransactionFragment

class BsitFragmentPagerAdapter(
    fragment: FragmentActivity,
    private val bundle: List<Bundle>
) : FragmentStateAdapter(
    fragment
){
    private val fragment = listOf(
        ::TransactionFragment,
        ::ContactsFragment,
        ::ProfileFragment
    )
    override fun getItemCount() = fragment.size

    override fun createFragment(position: Int): Fragment {
        val fragmentName = this.fragment.getOrNull(position)
        val fragmentInstance = fragmentName?.invoke() ?: Fragment()

        val bundle = bundle.getOrNull(position)
        fragmentInstance.arguments = bundle

        return fragmentInstance
    }

}