package com.yusuf.bsit.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yusuf.bsit.databinding.ItemContactsBinding
import com.yusuf.bsit.model.ContactResponse

class ContactsAdapter(
) : RecyclerView.Adapter<ContactsAdapter.ContactsViewHolder>() {
    inner class ContactsViewHolder(private val binding: ItemContactsBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindView(item: ContactResponse) {
            binding.ccTvName.text = item.name
            binding.ccTvNumber.text = item.noTelp
            binding.ivCall.setOnClickListener {
                onClickPhone.invoke(item) }
        }

    }

    private var data: MutableList<ContactResponse> = mutableListOf()
    private var onClickPhone: (ContactResponse) -> Unit = {}

    fun  setData(newData: MutableList<ContactResponse>){
        data =newData
        notifyDataSetChanged()
    }
    fun onClickIconCall(clickContact : (ContactResponse)->Unit){
        onClickPhone = clickContact
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ContactsViewHolder(
        ItemContactsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: ContactsViewHolder, position: Int) {
        holder.bindView(data[position])
    }
}