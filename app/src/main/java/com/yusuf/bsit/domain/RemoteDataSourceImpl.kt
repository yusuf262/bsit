package com.yusuf.bsit.domain

import com.yusuf.bsit.model.ContactResponse
import com.yusuf.bsit.model.ProfileResponse
import com.yusuf.bsit.model.TransactionResponse
import retrofit2.Response
import javax.inject.Inject

class RemoteDataSourceImpl @Inject constructor(
    private val service: Service): RemoteDataSource {
    override suspend fun getTransaction(): Response<List<TransactionResponse>> {
        return service.getTransaction()
    }

    override suspend fun getContact(): Response<List<ContactResponse>> =
        service.getContact()

    override suspend fun getProfile(): Response<ProfileResponse> =
        service.getProfile()
}