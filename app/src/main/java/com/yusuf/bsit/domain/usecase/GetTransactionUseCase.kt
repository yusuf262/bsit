package com.yusuf.bsit.domain.usecase

import com.yusuf.bsit.domain.repository.Repository
import com.yusuf.bsit.model.TransactionResponse
import retrofit2.Response
import javax.inject.Inject

class GetTransactionUseCase @Inject constructor(
    private val repository: Repository
) {
    suspend fun getTransaction(): Response<List<TransactionResponse>> {
        return repository.getTransaction()
    }
}