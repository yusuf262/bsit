package com.yusuf.bsit.domain.usecase

import com.yusuf.bsit.domain.repository.Repository
import com.yusuf.bsit.model.ContactResponse
import com.yusuf.bsit.model.ProfileResponse
import com.yusuf.bsit.model.TransactionResponse
import retrofit2.Response
import javax.inject.Inject

class GetProfileUseCase @Inject constructor(
    private val repository: Repository
) {
    suspend fun getProfile(): Response<ProfileResponse> {
        return repository.getProfile()
    }
}