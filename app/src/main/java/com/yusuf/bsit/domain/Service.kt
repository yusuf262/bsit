package com.yusuf.bsit.domain

import com.yusuf.bsit.model.ContactResponse
import com.yusuf.bsit.model.ProfileResponse
import com.yusuf.bsit.model.TransactionResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface Service {

    @GET("transaction")
    suspend fun getTransaction(): Response<List<TransactionResponse>>

    @GET("contact")
    suspend fun getContact(): Response<List<ContactResponse>>

    @GET("profile")
    suspend fun getProfile(): Response<ProfileResponse>

}