package com.yusuf.bsit.domain.usecase

import com.yusuf.bsit.domain.repository.Repository
import com.yusuf.bsit.model.ContactResponse
import retrofit2.Response
import javax.inject.Inject

class GetContactUseCase @Inject constructor(
    private val repository: Repository
) {
    suspend fun getContact(): Response<List<ContactResponse>> {
        return repository.getContact()
    }
}