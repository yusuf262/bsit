package com.yusuf.bsit.domain

import com.yusuf.bsit.model.ContactResponse
import com.yusuf.bsit.model.ProfileResponse
import com.yusuf.bsit.model.TransactionResponse
import retrofit2.Response

interface RemoteDataSource {

    suspend fun getTransaction(): Response<List<TransactionResponse>>
    suspend fun getContact(): Response<List<ContactResponse>>
    suspend fun getProfile(): Response<ProfileResponse>
}